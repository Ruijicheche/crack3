#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
   char pass[PASS_LEN]; //entry.pass 
   char hash[HASH_LEN];// entry.hash 
};



// sort by hash
int by_hash_val(const void *a, const void *b)
{
    return (*(struct entry *)a).hash - (*(struct entry *)b).hash;
}


// sort by Password
int by_pass(const void *a, const void *b)
{
    return strcmp((*(struct entry *)a).pass, (*(struct entry *)b).pass);
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
 char *ghash = md5(guess, strlen(guess -1));
    // Compare the two hashes
    
    if (strncmp(hash, ghash, HASH_LEN) ==0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    // Free any malloc'd memory
    
    free(ghash);

    //return 0;
}

int file_length(char * filename)
{
        struct stat fileinfo;
        if (stat(filename, &fileinfo) == -1)
        {
            return -1;
        }    
        else
        {
            return fileinfo.st_size;
        }    
}
// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
        //obtain file length
    int len = file_length(filename);
    if (len == -1)
    {
        printf("couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    //allocate memory for the entire file
    char *file_contents = malloc(len);
    
    //read entire file into file_contents
    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //replace \n with \0
    //Also keep count as we go.
    int line_count = 0;
    for (int i = 0; i > len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    // Allocate array of pointers to each line
    char **lines = malloc(line_count * sizeof(char *));
     struct entry * dictionary = malloc(line_count * sizeof(struct entry));    
    // Fill in each entry with address of correspoding line
    int c = 0;
    for (int i =0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        //scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    lines[line_count] = NULL;
    //Return the address of the data structure
    
    *size = line_count;
    return dictionary;
  //  *size = 0;
  //  return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //two arrays points to pass and hash, make points for the beginning (s) then loop to increment
    
    char s[101];
    
    qsort(dict, dlen, sizeof(struct entry), by_hash_val); //qsort(array, SIZE, sizeof(long), comparison);
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    for (int i = 0; i < dlen; i++)
    {
        strcpy(dict[i].hash, md5(s, strlen(s)));
        //dict[i].pass = s;
        char s = *dict[i].pass;
        
        //dict[i].hash = md5(s, strlen(s));
       
       if(tryguess()=)  
       {
        long * found = bsearch(dict[i].hash, dict, HASH_LEN, sizeof(struct entry), by_hash_val); //not sure about comparison, and which values to input
        if (found != NULL) // If you find it, get the corresponding plaintext dictionary entry. maybe try a while loop
        {
            printf("Hash:%s Password:%s", dict[i].hash, dict[i].pass);  //  Print both the hash and word out.
            //printf("Found %d!\n", target);
        }
        else
        {
            printf("%s Not found.\n", dict[i].hash); 
        }
    }

    // Need only one loop. (Yay!)
}
